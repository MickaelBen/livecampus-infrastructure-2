package internal

import "fmt"

func main(){
	result, err := Division(10.0,0)

	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}

func Division(first, second float32) (float32, error) {
	if second == 0 {
		return 0, fmt.Errorf("division by 0 exception")
	}

	return first / second, nil
}