package internal

import "testing"

var tests = []struct {
	name string
	first float32
	second float32
	want float32
	isErr bool
}{
	{"valid", 100.0, 4.0, 25.0, false},
	{"invalid", 100.0, 0.0, 0.0, false},
	{"cas3", 2.0, 2.0, 4.0, false},
	

}

func TestDivision(t *testing.T) {
	for _, tt := range tests {
		want, err := Division(tt.first, tt.second)
		if (err != nil) != tt.isErr {
			t.Errorf("%s: got error %v, want error %v", tt.name, err, tt.isErr)
		}
		if want != tt.want {
			t.Errorf("%s: got %v, want %v", tt.name, want, tt.want)
		}
	}
}